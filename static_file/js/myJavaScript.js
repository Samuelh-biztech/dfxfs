var MovingMessageText; 
var titleFontSize = 30;
var currencyFontSize = 23;
var ColumnHeight = 60;
var ImageHeight = 60; //flag image height
var ColumnFontColour = ["black","black","black","black","black","#black"];
var ColumnWidth = ["0","60","110","380","182","182"];
var CurrencyCount; //based on data.xml
var CurrencyCountDivide2; //based on data.xml
var ColumnCountPerTable = 6;
var Currency_array = new Array(CurrencyCount);
var xmlDoc;
var Current_fileTime;
var XML_filename = "data.xml";
var lanSetting = 1;

////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// button onClick ///////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
function send() {
	saveInput2XML();
	xmlhttpPost("createXML.php", XML_filename); //save xml to a file
	location.reload(); //reload current web page
}

function saveAs() {
	saveInput2XML();
	filename = "backup/" + createFilename_accordingDate() + ".xml";
	xmlhttpPost("createXML.php", filename); //save xml to a file
}


function clearCurrencyData() {
	blankCurrencyData("table1");
	blankCurrencyData("table2");
}


function fill_ExchangeRate_toInputBox() {
	fill_ExchangeRate_toInputTextBox("table1");
	fill_ExchangeRate_toInputTextBox("table2");
}

////////////////////////////////////////////////////////////////////////////////////
//////////////////////////// run this function when webpage start load /////////////
////////////////////////////////////////////////////////////////////////////////////
function clientAutoRun() {
	ColumnWidth = ["0","0","0","120","68","68"];
	titleFontSize = 14;
	currencyFontSize =14;
	ColumnHeight = 40;
	ImageHeight = ColumnHeight ;
	commonStartup();
	insertInput("table1");
	insertInput("table2");
	fill_ExchangeRate_toInputBox();
	// document.getElementById("movingMessage").value= MovingMessageText;
	setFocus("textBox1");
}


function serverAutoRun() {
	setInterval(function(){showDateTime()},500); //run showDateTime function every second
	Current_fileTime= getFiletime();
	setInterval(function(){fileIsModified()},5000); //run fileIsModified function every 5 seconds
	setInterval(function(){ChangeLangage()},7000);
	commonStartup();


	
	// document.getElementById("movingMessage").innerHTML= MovingMessageText; 
	textAnimate();
}

function ChangeLangage(){
	if(lanSetting == '1')
	{
	
		
		lanSetting = '2';
		document.getElementById("t1r1").innerHTML = 'ሀገር';
		document.getElementById("t1r3").innerHTML = 'ኮድ';
		document.getElementById("t1r4").innerHTML = 'ገንዘብ';
		document.getElementById("t1r5").innerHTML = 'የመሸጫ ዋጋ';
		document.getElementById("t1r6").innerHTML = 'የመግዣ ዋጋ';
		
		document.getElementById("t2r1").innerHTML = 'ሀገር';
		document.getElementById("t2r3").innerHTML = 'ኮድ';
		document.getElementById("t2r4").innerHTML = 'ገንዘብ';
		document.getElementById("t2r5").innerHTML = 'የመሸጫ ዋጋ';
		document.getElementById("t2r6").innerHTML = 'የመግዣ ዋጋ';
		
		change("am");
		return 0;
	}
	else if(lanSetting == '2')
	{

		//XML_filename = "data.xml";

		lanSetting = '1';
		document.getElementById("t1r1").innerHTML = 'Country';
		document.getElementById("t1r3").innerHTML = 'Code';
		document.getElementById("t1r4").innerHTML = 'We Sell';
		document.getElementById("t1r5").innerHTML = 'We Sell';
		document.getElementById("t1r6").innerHTML = 'We Buy';
		
		document.getElementById("t2r1").innerHTML = 'Country';
		document.getElementById("t2r3").innerHTML = 'Code';
		document.getElementById("t2r4").innerHTML = 'Currency';
		document.getElementById("t2r5").innerHTML = 'We Sell';
		document.getElementById("t2r6").innerHTML = 'We Buy';
		change("en");
		return 0;

	}
}

function commonStartup() {
	document.bgColor="white"; //set background to black
	loadSettings();	//load moving message & currencies

	setTitleStyle("table1");	
	setTitleStyle("table2");

	//create table
	for (rowNum=0; rowNum<CurrencyCountDivide2; rowNum++) {	
	addRow("table1");
	addRow("table2");
	}	
	
	//display image
	displayCurrencyImage("table1");
	displayCurrencyImage("table2");

	//display currrency 
	displayCurrencyData("table1");
	displayCurrencyData("table2");	
}

function commonStartup() {
	document.bgColor="white"; //set background to black
	loadSettings("en");	//load moving message & currencies

	setTitleStyle("table1");	
	setTitleStyle("table2");

	//create table
	for (rowNum=0; rowNum<CurrencyCountDivide2; rowNum++) {	
	addRow("table1");
	addRow("table2");
	}	
	
	//display image
	displayCurrencyImage("table1");
	displayCurrencyImage("table2");

	//display currrency 
	displayCurrencyData("table1");
	displayCurrencyData("table2");	
}
function change(str1) {
	
	document.bgColor="white"; //set background to black
	var strg  = str1;
	loadSettings(strg);	//load moving message & currencies

	

	//display currrency 
	displayCurrencyData("table1");
	displayCurrencyData("table2");	
}

////////////////////////////////////////////////////////////////////////////////////
//////////////////////// timer function ////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
// show system data & time
function showDateTime() {
	dateTimeNow = new Date(); 

	weekday = new Array(7);
	weekday[0]=  "Sun";
	weekday[1] = "Mon";
	weekday[2] = "Tue";
	weekday[3] = "Wed";
	weekday[4] = "Thu";
	weekday[5] = "Fri";
	weekday[6] = "Sat";
	weekdayNow = weekday[dateTimeNow.getDay()];
	
	dd = dateTimeNow.getDate(); 
	mm = dateTimeNow.getMonth()+1;//January is 0! 
	yyyy = dateTimeNow.getFullYear();
	yyyy = yyyy.toString().substr(2,2); 
	if(dd<10){dd='0'+dd};
	if(mm<10){mm='0'+mm}; 
	dateNow= dd + "/" + mm + "/" + yyyy + " G.C";
	// var cal = " G.C";
	var prefix = "";
	//var gcfulldate =  
	document.getElementById("dateNow").innerHTML = prefix.concat(dateNow);
	
	hh = dateTimeNow.getHours();
	nn = dateTimeNow.getMinutes();
	ss = dateTimeNow.getSeconds();
	if(hh<10){hh='0'+hh};
	if(nn<10){nn='0'+nn}; 	
	if(ss<10){ss='0'+ss}; 	
	timeNow = hh + ":" + nn + ":" + ss;
	document.getElementById("timeNow").innerHTML = timeNow; 
}


function fileIsModified() {
	lastModified_filetime= getFiletime();
	if 	(lastModified_filetime != Current_fileTime) {
		location.reload(); //reload current web page
		//Current_fileTime= lastModified_filetime;
		//loadSettings(); //load moving message & currencies
		//display currrency 
		//displayCurrencyData("table1");
		//displayCurrencyData("table2");
	}	
}


// retrieve last modified time for a file
function getFiletime() {
	xmlhttp= create_XMLHttpRequest();
	xmlhttp.open("GET","fileLastModified.php" ,false);
	xmlhttp.send();
	txt=xmlhttp.responseText; 
	return txt;
}


////////////////////////////////////////////////////////////////////////////////////
// add one row to table according to existing table's column & width
////////////////////////////////////////////////////////////////////////////////////
function addRow(table) {
	table = document.getElementById(table);
	footer = table.createTFoot();
	row = footer.insertRow(0); //loop through all cell in a row (always row0)
	for (colNum=0; colNum < ColumnCountPerTable; colNum++) {	
		cell = row.insertCell(colNum);
		//cell.innerHTML = "<b>This is a table footer</b>";
	}
}


// read data from xml file & store it to array
function loadSettings(lang) {	
	xmlDoc=loadXMLdoc();
	
	// load moving message
	//x=xmlDoc.getElementsByTagName("MOVING_MESSAGE");
	//MovingMessageText= x[0].getElementsByTagName("MESSAGE_TEXT")[0].childNodes[0].nodeValue;
			
	// load currencies
	x=xmlDoc.getElementsByTagName("COUNTRY");
	CurrencyCount= x.length;
	CurrencyCountDivide2= (CurrencyCount/2);	
	for (i=0;i<CurrencyCount;i++)
	{ 
		Currency_array[i] = new Array(CurrencyCount);
		
		Currency_array[i][0]=x[i].getElementsByTagName("AMOUNT")[0].childNodes[0].nodeValue;
		Currency_array[i][1]=x[i].getElementsByTagName("CURRENCY_CODE")[0].childNodes[0].nodeValue;
		if(lang=="am"){
			if(i==0)
				Currency_array[i][2]="የአምሪካ ዶላር";
			else if (i==1)
				Currency_array[i][2]="ዩ.አ.ኤ ድርሀም";
			else if (i==2)
				Currency_array[i][2]="ዩሮ";
			else if (i==3)
				Currency_array[i][2]="የኢንግሊዝ ፓዉንድ";
			else if (i==4)
				Currency_array[i][2]="የን ሬንሚንቢ";
			else if (i==5)
				Currency_array[i][2]="የደ.አፍሪካ ራንድ";
			else if (i==6)
				Currency_array[i][2]="የኬኒያ ሽልንግ";
			else if (i==7)
				Currency_array[i][2]="የጅቡቲ ፍራንክ";
			else if (i==8)
				Currency_array[i][2]="የህንድ ሩፒ";
			else if (i==9)
				Currency_array[i][2]="የኖርዌይ ክሮን";
			else if (i==10)
				Currency_array[i][2]="የካናዳ ዶላር";
			else if (i==11)
				Currency_array[i][2]="የሳውዲ ሪያል";
			else if (i==12)
				Currency_array[i][2]="የአውስትራሊያ ዶላር";
			else if (i==13)
				Currency_array[i][2]="የጃፓን የን";
			else if (i==14)
				Currency_array[i][2]="የዴንማርክ ክሮን";
			else if (i==15)
				Currency_array[i][2]="የስዊዝ ፍራንክ";
			else if (i==16)
				Currency_array[i][2]="የስዊዲን ክሮና";
			else if (i==17)
				Currency_array[i][2]="ኤስ ዲ አር";
			
	}
	else 
	{
		//alert(lang + "en");
		Currency_array[i][2]=x[i].getElementsByTagName("CURRENCY_NAME")[0].childNodes[0].nodeValue;
	}
		Currency_array[i][3]=x[i].getElementsByTagName("WE_BUY")[0].childNodes[0].nodeValue;  
		Currency_array[i][4]=x[i].getElementsByTagName("WE_SELL")[0].childNodes[0].nodeValue; 
		Currency_array[i][3]=fillZero_onRight(Currency_array[i][3]);
		Currency_array[i][4]=fillZero_onRight(Currency_array[i][4]);
	}
}

function fillZero_onRight(input) { 
	noOfDecimal = 4;
	leftCharacterCount_beforeDecimal = input.indexOf("."); //left character count before decimal
	if (leftCharacterCount_beforeDecimal == -1 ) { //if it is an integer
		input= input + "." //add a dot after the number
		leftCharacterCount_beforeDecimal = input.indexOf("."); //search the position again
	}
	noOfPattern_toBeAdd =(noOfDecimal+1)-(input.length-leftCharacterCount_beforeDecimal);
	input=input +repeat("0",noOfPattern_toBeAdd);
	return input; 
}


function repeat(pattern, count) {
    if (count < 1) return '';
    result = '';
    while (count > 0) {
        if (count & 1) result += pattern;
        count >>= 1, pattern += pattern;
    }
    return result;
}



// set table title (row 0)
function setTitleStyle(table) { 
	table = document.getElementById(table);   
	rows = table.getElementsByTagName("tr");   
	rows[0].className = "desc"; 
	rows[0].style.backgroundColor="#53251b";
	// rows[0].style.fontSize = titleFontSize+"px";
	//rows[0].style.fontFamily="Times New Roman";
	rows[0].style.color= "#ffffff";
	rows[0].style.textAlign = 'center';
	//rows[0].height = 50 + "px";
	rows[0].style.fontWeight = "bold";
	
}


// display image in column 0
function displayCurrencyImage(table) {
	for (rowNum=1; rowNum<=CurrencyCountDivide2; rowNum++) {
		x = document.getElementById(table).rows[rowNum].cells;
		img = document.createElement('img');
		imageIndex = rowNum-1;
		if (table=="table2") {
			imageIndex = imageIndex + CurrencyCountDivide2;
		}
		img.src = "./images/currency"+imageIndex+".png";
		img.height = ImageHeight;
		img.align="middle";
		x[0].appendChild(img); //first column
	}
}


// display data in column 1 to column 4
function displayCurrencyData(table) {
	for (rowNum=1; rowNum <= CurrencyCountDivide2; rowNum++) {	
		x = document.getElementById(table).rows[rowNum].cells;
		for (colNum=1; colNum < ColumnCountPerTable; colNum++) {	
			tableNum = 0;
			if (table=="table2") {
					tableNum= CurrencyCountDivide2;
			}	
			
			x[colNum].innerHTML=Currency_array[rowNum+tableNum-1][colNum-1];
			x[colNum].style.color= ColumnFontColour[colNum];
			 x[colNum].width= ColumnWidth[colNum];
			//x[colNum].style.fontFamily="Times New Roman";
			 x[colNum].height = ColumnHeight + "px";
			x[colNum].style.fontSize=currencyFontSize + "px";
			x[2].style.fontWeight= "600";
			 // x[0].width= ColumnWidth[0];
			 // x[1].width= ColumnWidth[1];
			 //  x[2].width= ColumnWidth[2];
			 //  x[3].width= ColumnWidth[3];
			 //  x[4].width= ColumnWidth[4];
			 //  x[5].width= ColumnWidth[5];
			 x[4].style.fontSize="180%";
			 x[5].style.fontSize="180%";
		}

	}		
}


//create text box for input
function insertInput(table) {
	if (table=="table1") {
		textBoxtabIndex = 0;
	} else {	
		textBoxtabIndex = CurrencyCount;
	}
	for (rowNum=1; rowNum <= CurrencyCountDivide2; rowNum++) {
		x = document.getElementById(table).rows[rowNum].cells; 
		for (col=4; col<=5; col++) { //create text input box in column 4 & 5
			textBoxtabIndex = textBoxtabIndex + 1;
			x[col].innerHTML = '<input type="text" style="width:60px" maxlength="8" onkeyup="isNumber(this)" onkeypress="enterAsTab(this,event)"/>'; 
			//textInputId= "textBox" + textBoxtabIndex;
			x[col].children[0].id="textBox" + textBoxtabIndex;
			x[col].children[0].tabIndex=textBoxtabIndex;
		}
	}
}


function fill_ExchangeRate_toInputTextBox(table) {
	for (rowNum=1; rowNum <= CurrencyCountDivide2; rowNum++) {	 //loop through all rows
		x = document.getElementById(table).rows[rowNum].cells; 
		tableNum = 0;
		if (table=="table2") {
			tableNum= CurrencyCountDivide2;
		}	
		for (col=4; col<=5; col++) { //clear data from column 4 & 5
			x[col].children[0].value =Currency_array[rowNum+tableNum-1][col-1];
			//x[4].children[0].value =Currency_array[rowNum+tableNum-1][3];
		}	
	}
}


// clear input box
function blankCurrencyData(table) {
	for (rowNum=1; rowNum <= CurrencyCountDivide2; rowNum++) {	
		x = document.getElementById(table).rows[rowNum].cells; 
		for (col=4; col<=5; col++) { //clear data from column 4 & 5
			x[col].children[0].value =""; 
		}	
	}
}


// store user input to xml
function saveInput2XML() {
	/*
	mess=xmlDoc.getElementsByTagName("MOVING_MESSAGE");
	message_text=mess[0].getElementsByTagName("MESSAGE_TEXT")[0].childNodes[0];
	message_text.nodeValue= document.getElementById("movingMessage").value;
	*/
	x=xmlDoc.getElementsByTagName("COUNTRY");
	for (i=0;i<CurrencyCount;i++)
	{ 
		if (i < CurrencyCountDivide2) {
			table= "table1";
			current_row = i+1;
		} 
		else {
			table= "table2";
			current_row = i+1-CurrencyCountDivide2;
		}
		myInput = document.getElementById(table).rows[current_row].cells; 
		
		we_buy= x[i].getElementsByTagName("WE_BUY")[0].childNodes[0];  
		we_buy.nodeValue= myInput[4].children[0].value; //column 4

		we_sell=x[i].getElementsByTagName("WE_SELL")[0].childNodes[0]; 
		we_sell.nodeValue= myInput[5].children[0].value; //column 5
	}
	//alert (x[10-1].getElementsByTagName("WE_SELL")[0].childNodes[0].nodeValue);
}

// convert xml to string
function xmlToString(xmlDoc){
	if(xmlDoc.xml){
		// MSIE
		xmlString = xmlDoc.xml;
	}else{
		xmlString = (new XMLSerializer).serializeToString(xmlDoc);
	}
	return xmlString;
}

// create an XMLHttpRequest object
function create_XMLHttpRequest() {
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	return xmlhttp;
}


// load xml file using http GET request
function loadXMLdoc() {
	xmlhttp= create_XMLHttpRequest();
	xmlhttp.open("GET",XML_filename ,false);
	xmlhttp.send();
	return xmlhttp.responseXML; 
}	


// http POST request
function xmlhttpPost(strURL, filename) {
	xmlhttp= create_XMLHttpRequest();
//filename="s"+filename;
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {	
		responseTextLength = xmlhttp.responseText.length;
			if (responseTextLength<6) {
				alert("FX information Updated Successfully."); 
			} else {
				alert("Saving " + filename + " failed.");
			}
		}
	}

    xmlhttp.open('POST', strURL, false);
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	postMessage = "xml=" + xmlToString(xmlDoc) + "&filename=" + filename;
    xmlhttp.send(postMessage);
}


// create a filename based on date (eg. 2014_0502)
function createFilename_accordingDate() {
	today = new Date(); 
	dd = today.getDate(); 
	mm = today.getMonth()+1;//January is 0! 
	yyyy = today.getFullYear(); 
	if(dd<10){dd='0'+dd} ;
	if(mm<10){mm='0'+mm}; 
	return (yyyy + "_" + mm + dd); 
} 


// only accept numeric input
function isNumber(obj) { 
    allowedCharacters = /^[0-9-'.'-',']*$/; 
    if (!allowedCharacters.test(obj.value)) { 
            obj.value = obj.value.replace(/[^0-9-'.'-',']/g,""); 
        } 
}

//enter as tab
function enterAsTab(obj,e) { 
   e=(typeof event!='undefined')?window.event:e;// IE : Moz 
   if(e.keyCode==13){ 
	currrenctTabIndex = obj.tabIndex;
	nextTabIndex = currrenctTabIndex + 1;
	if (nextTabIndex > CurrencyCount*2) {
		nextTabIndex = 1;
	}
	nextTextBoxID = "textBox" + nextTabIndex;
	currentElement = document.getElementById(nextTextBoxID);
	currentElement.focus();
	currentElement.select();
   } 
}


// focus a control
function setFocus(element) {
	currentElement = document.getElementById(element);
	currentElement.focus();
	//currentElement = document.getElementById("table1").rows[1].cells; //point to table row 1
	//currentElement[4].children[0].focus(); //set focus to column 4
	//currentElement[4].children[0].select();
}

