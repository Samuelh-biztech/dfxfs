#!/usr/bin/env python
from flask import *
import sqlite3 as sql
from flask_socketio import SocketIO, emit, join_room, leave_room, \
    close_room, rooms, disconnect

async_mode = None

conn=sql.connect('currency.db')
cur=conn.cursor()
cur.execute('select *from currency')
rows=cur.fetchall()
conn.close()
app = Flask(__name__)

app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, async_mode=async_mode)
thread = None

@app.route('/')
def index():
    conn=sql.connect('currency.db')
    cur=conn.cursor()
    cur.execute('select *from currency')
    rows=cur.fetchall()
    conn.close()
    return render_template('inet.html',rows=rows)

@app.route('/list')
def list():
 con=sql.connect("currency.db")
 con.row_factory = sql.Row
 cur = con.cursor()
 cur.execute("select * from currency")
 rows=cur.fetchall();
 conn.close()
 return render_template("list.html",rows=rows)


@app.route('/master')
def ind():
    return render_template('index.html', async_mode=socketio.async_mode)


@app.route('/three')
def inde():
    return render_template('inde2.html', async_mode=socketio.async_mode)


@socketio.on('my_event', namespace='/test')
def test_message(message):
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my_response',
         {'data': message['data'], 'count': session['receive_count']})


@socketio.on('my_broadcast_event', namespace='/test')
def test_broadcast_message(message):
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my_response',
         {'data': message['data'], 'count': session['receive_count']},
         broadcast=True)

@socketio.on('add', namespace='/test')
def test_messages(message):
    print message['dollarbuying']
    try:
     dollarbuying=message['dollarbuying']
     dollarselling=message['dollarselling']
     poundbuying=message['poundbuying']
     poundselling=message['poundselling']
     eurobuying=message['eurobuying']
     euroselling=message['euroselling']
     pin=0.0
     with sql.connect("currency.db") as con:
      cur = con.cursor()
      cur.execute("UPDATE currency set selling=? where name=?",(dollarselling,'USD'))
      cur.execute("UPDATE currency set buying=? where name=?",(dollarbuying,'USD'))

      cur.execute("UPDATE currency set selling=? where name=?",(poundselling,'GBP'))
      cur.execute("UPDATE currency set buying=? where name=?",(poundbuying,'GBP'))

      cur.execute("UPDATE currency set selling=? where name=?",(euroselling,'Euro'))
      cur.execute("UPDATE currency set buying=? where name=?",(eurobuying,'Euro'))
      con.commit()
      msg= "Record successfully added"
    except:
     con.rollback()
     msg= "error in insert operation"
    finally:
     con.close()
    


@socketio.on('join', namespace='/test')
def join(message):
    join_room(message['room'])
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my_response',
         {'data': 'In rooms: ' + ', '.join(rooms()),
          'count': session['receive_count']})


@socketio.on('leave', namespace='/test')
def leave(message):
    leave_room(message['room'])
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my_response',
         {'data': 'In rooms: ' + ', '.join(rooms()),
          'count': session['receive_count']})


@socketio.on('close_room', namespace='/test')
def close(message):
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my_response', {'data': 'Room ' + message['room'] + ' is closing.',
                         'count': session['receive_count']},
         room=message['room'])
    close_room(message['room'])


@socketio.on('my_room_event', namespace='/test')
def send_room_message(message):
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my_response',
         {'data': message['data'], 'count': session['receive_count']},
         room=message['room'])


@socketio.on('disconnect_request', namespace='/test')
def disconnect_request():
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my_response',
         {'data': 'Disconnected!', 'count': session['receive_count']})
    disconnect()


@socketio.on('my_ping', namespace='/test')
def ping_pong():
    emit('my_pong')


@socketio.on('connect', namespace='/test')
def test_connect(): 
    emit('my_response', {'datas': 'Connected', 'count': 0})


@socketio.on('disconnect', namespace='/test')
def test_disconnect():
    print('Client disconnected', request.sid)


if __name__ == '__main__':
    socketio.run(app, debug=True,host='0.0.0.0',port=2000)
    